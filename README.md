PHREEQC Web Interface
=====================

An extensible interface for executing PHREEQC calculations through a web
interface.

## Structure

- PHREEQC webrunner: takes jobs through a REST API and runs them, saving the
  output to the database
- PHREEQC template runner (`api-server`): generates, submits, and stores parsed
  results for job templates through a REST API
- PHREEQC frontend: very basic frontend for submitting jobs and viewing results
