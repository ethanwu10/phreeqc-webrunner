const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const P = require('bluebird')
const path = require('path')
const mongoose = require('mongoose')
const config = require('config')
const _ = require('lodash')
const phreeqc = require('./lib/phreeqc')
const runSchema = require('./lib/runSchema')
const DataGather = require('./lib/DataGather')

let app = express()

app.use(morgan('dev'))

let RunModel
let dataGather
mongoose.Promise = P
mongoose.connect(config.get('mongo.server'), {useMongoClient: true}) // FIXME pls
  .then(function () {
    RunModel = mongoose.model('Run', runSchema)
    dataGather = new DataGather(RunModel)
  })

function methodNotAllowed (req, res, next) {
  let err = new Error('Method Not Allowed')
  err.status = 405
  next(err)
}

app.route('/run')
.post(bodyParser.raw({type: ['application/octet-stream', 'application/x-phreeqc-input']}), function (req, res, next) {
  phreeqc(req.body)
    .then(function (data) {
      let dirPath = data[0]
      let database = data[1]
      console.log('collect')
      return dataGather.gather(dirPath, [database])
    })
    .then(function (runDoc) {
      // let dirPath = data[0]
      // let runDoc = data[1]
      // return P.all([P.resolve(dirPath), runDoc.save()])
      return runDoc.save()
    })
    .then(function (runDoc) {
      console.log('Success')
      res.statusCode = 201
      res.set('Location', '/run/' + runDoc._id)
      res.end()
    })
    .catch(function (err) {
      console.log(err)
      if (err.code === phreeqc.ERR_PHREEQC_RUN_ERROR) {
        res.statusCode = 400
        res.json(err.errDoc)
      } else {
        res.statusCode = 500
      }
      res.end()
    }).done()

  // res.status(200)
  // res.json(req.body)
  // res.end()
})
.all(methodNotAllowed)

function getDocument (id) {
  return function (req, res, next) {
    req.document = RunModel.findOne({_id: _.get(req, id || 'params.id')})
    next()
  }
}

app.route('/run/:id')
  .get(getDocument(), function (req, res) {
    req.document
      .select('files.name')
      .then(function (run) {
        if (run) {
          res.statusCode = 200
          res.json(run.files.map((file) => {
            return file.name
          }))
        } else {
          res.statusCode = 404
          res.end()
        }
      })
      .catch(function (err) {
        console.log(err)
        res.statusCode = 500
        res.end()
      })
  })
  .delete(getDocument(), function (req, res) {
    req.document
      .remove()
      .then(function (run) {
        res.statusCode = 201
        res.end()
      })
      .catch(function (err) {
        console.log(err)
        res.statusCode = 500
        res.end()
      })
  })
  .all(methodNotAllowed)
app.route(/\/run\/([^/]*)\/(.*)/) // FIXME: not at all an atrocious mess
  .get(getDocument('params.[0]'), function (req, res) {
    req.document
      // .where(function (run) {
      //   return !!(_.find(run.files, function (file) {
      //     return file.name === req.params[1]
      //   }))
      // })
      .select('files.name files.contents')
      .then(function (run) {
        let file
        if (run && (file = _.find(run.files, function (file) {
          return file.name === req.params[1]
        }))) {
          console.log(file)
          res.statusCode = 200
          res.end(file.contents.toString())
        } else {
          res.statusCode = 404
          res.end()
        }
      })
  })
  .all(methodNotAllowed)

app.use(function (req, res, next) { // Catch 404
  let err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use(function (err, req, res, next) { // Error handler
  console.log(err)
  res.status(err.status || 500)
  res.end(err.message) // TODO: keep?
})

module.exports = app
