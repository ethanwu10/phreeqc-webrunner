const P = require('bluebird')
const _ = require('lodash')
const child = require('child_process')
const path = require('path')
const assert = require('assert')
const temp = P.promisifyAll(require('temp'))
const fs = require('fs-extra')
const config = require('config')

const ERR_PHREEQC_RUN_ERROR = 1001

temp.dir = '/var/tmp'

function validate (input) {
  assert(Buffer.isBuffer(input), 'Input not a buffer')
  assert(!input.includes('../'), '../ not allowed') // TODO: Allow commented, disallow absolute
  // assert(input.includes('DATABASE'), 'No DATABASE selected') // TODO: Allow multiple databases
  return input
}

function getDatabases (input) {
  assert(Buffer.isBuffer(input), 'Input not a buffer') // TODO: Necessary?
  let index = input.indexOf('DATABASE') // Only 1 DATABASE per file
  if (index !== -1) {
    index += 'DATABASE'.length
    for (; input[index] === ' '.charCodeAt(0) || input[index] === '\t'.charCodeAt(0); ++index) {
    }
    let start = index
    for (; input[index] !== ' '.charCodeAt(0) && input[index] !== '\t'.charCodeAt(0) && input[index] !== '\n'.charCodeAt(0) && input[index] !== '\r'.charCodeAt(0); // FIXME: <==
           ++index) {
    }
    return input.toString('utf8', start, index) // TODO: Encodings (ex. PHREEQC examples are ISO8859
  } else {
    return null
  }
}

function runPhreeqc (wd, db, phreeqcPath) {
  let getError = function (errDoc) {
    let e = new Error('PHREEQC Run error', getError)
    e.code = ERR_PHREEQC_RUN_ERROR
    e.errDoc = errDoc
    return e
  }
  const isDefaultDb = db.includes('!')
  if (isDefaultDb) db = db.slice(0, db.length - 1)
  return P.method(function () {
    if (isDefaultDb) return
    console.log('copying')
    console.log(path.join(wd, path.basename(db)))
    return fs.copy(db, path.join(wd, path.basename(db))).then(function () {})
      .catch(function (err) {
        if (err.code === 'ENOENT') {
          throw getError({reason: 'DB_NOT_FOUND', db: err.path})
        } else {
          throw err
        }
      })
  })().then(function () {
    let args = ['in', 'out']
    if (isDefaultDb) args.push(db)

    return new Promise(function (resolve, reject) {
      let phreeqc = child.spawn(phreeqcPath, args, {cwd: wd})
      phreeqc.on('error', function (err) {
        reject(getError({reason: 'EXCEPTION', err: err}))
      })
      phreeqc.stderr.on('data', function (chunk) { // Error: die
        if (chunk.includes('ERROR')) {
          phreeqc.kill()
          reject(getError({reason: 'STDERR', stderr: chunk.toString()}))
        }
      })
      phreeqc.on('exit', function (code, signal) {
        if (code === 0) {
          console.log('exit')
          resolve() // TODO: pass data?
        } else {
          if (code) {
            reject(getError({reason: 'CODE', code: code}))
          } else if (signal) {
            reject(getError({reason: 'SIGNAL', signal: signal}))
          }
        }
      })
    })
  }).catch(function (err) {
    console.log('fail')
    let fRethrow = function () {
      return P.reject(err)
    }
    return fs.remove(wd).then(fRethrow, fRethrow)
  })
}

function phreeqc (input, options) {
  options = _.defaults(options, {
    dbDir: config.get('phreeqc.dbDir'), // path.join(__dirname, 'database'), // FIXME: load dir
    path: config.get('phreeqc.binPath') // path.resolve(__dirname, '..', 'phreeqc')
  })
  return P.method(validate)(input)
    .then(function (input) {
      return P.all([P.resolve(input), temp.mkdirAsync('phreeqc')])
    })
    .then(function (data) {
      let input = data[0]
      let dirPath = data[1]
      return P.all([P.resolve(input), P.resolve(dirPath), fs.writeFile(path.join(dirPath, 'in'), input, {mode: parseInt('600', 8)})])
    })
    .then(function (data) {
      let input = data[0]
      let dirPath = data[1]
      let database = getDatabases(input)
      let isDefaultDatabase = !database
      database = database || 'phreeqc.dat'
      return P.all([P.resolve(dirPath), P.resolve(database),
        runPhreeqc(dirPath, path.join(options.dbDir, database) + (isDefaultDatabase ? '!' : ''), options.path)]) // FIXME: passing
    })
    .catch(function (err) {
      if (err.code === ERR_PHREEQC_RUN_ERROR && err.errDoc.reason === 'DB_NOT_FOUND') {
        err.errDoc.db = path.relative(options.dbDir, err.errDoc.db) // Sanitize
      }
      throw err
    })
  // .then(function () {
    // return temp.cleanupAsync()
  // })
}

module.exports = phreeqc
module.exports.validate = validate
module.exports.ERR_PHREEQC_RUN_ERROR = ERR_PHREEQC_RUN_ERROR
