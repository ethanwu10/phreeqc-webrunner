const P = require('bluebird')
const walk = require('walk')
const path = require('path')
const fs = require('fs-extra')

class DataGather {
  constructor (RunModel) {
    this.RunModel = RunModel
  }

  gather (wd, exclude) { // TODO: Error handle all the things
    let obj = this
    exclude = exclude || []
    return new P(function (resolve, reject) {
      // let inputData, outputData
      // fs.readFile(path.join(wd, 'in'))
      //   .then(function (data) {
      //     inputData = data
      //     return fs.readFile(path.join(wd, 'out'))
      //   })
      //   .then(function (data) {
      //     outputData = data
      //   })
      let files = []
      let walker = walk.walk(wd)
      walker.on('file', function (root, fileStats, next) {
        let fileInfo = {}
        let semiAbsPath = path.join(root, fileStats.name)
        fileInfo.name = path.relative(wd, semiAbsPath)
        switch (fileInfo.name) {
          // case 'in':
          // case 'out':
          case 'phreeqc.log':
            return next()
          default:
        }
        if (exclude.includes(fileInfo.name)) return next()
        console.log(semiAbsPath)
        fs.readFile(semiAbsPath)
          .then(function (data) {
            fileInfo.contents = data
            files.push(fileInfo)
            next()
          })
      })
      // TODO: Error handling
      walker.on('end', function () {
        fs.remove(wd).then(function () {
          let runDoc = new obj.RunModel({
            // label: path.basename(wd),
            // input: inputData,
            // output: outputData,
            files: files
          })
          resolve(runDoc)
          // resolve([wd, runDoc])
        })
      })
    })
  }
}

module.exports = DataGather
