const mongoose = require('mongoose')
const Schema = mongoose.Schema

let runSchema = new Schema({
  // label: {type: String, required: true, unique: true},
  files: [{
    name: {type: String}, // TODO: should this be an index?
    contents: Buffer
  }]
  // database: [String] // TODO: Necessary?
})

module.exports = runSchema
