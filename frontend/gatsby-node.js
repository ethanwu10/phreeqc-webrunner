const webpack = require('webpack') // Pull Gatsby's webpack

exports.modifyWebpackConfig = ({config}) => {
  config.plugin('webpack-define', webpack.DefinePlugin, [{
    'global.GENTLY': false
  }])
}
