module.exports = {
  siteMetadata: {
    title: `PHREEQC Web Client`
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass'
  ],
  proxy: {
    prefix: '/api',
    url: 'http://localhost:3001'
  }
}
