import React from 'react'
import className from 'classnames'

class InputSpecies extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: null,
      isRange: false
    }

    this.onValueChange = this.onValueChange.bind(this)
    this.toggleRange = this.toggleRange.bind(this)
  }

  updateRange (vals) {
    for (let key in vals) {
      if (vals.hasOwnProperty(key)) {
        vals[key] = +vals[key]
      }
    }
    if (this.state.isRange) {
      let range = []
      if (+(vals.step) !== 0 && vals.max > vals.min) {
        let step = (vals.max - vals.min) / vals.step
        for (let val = vals.min; val <= vals.max; val += step) {
          range.push(val)
        }
      }
      this.props.onChange(range)
    } else {
      this.props.onChange([vals.value])
    }
  }

  onValueChange (ev) {
    let value = ev.target.value
    let partialState = {value: value}
    this.setState(partialState)
    if (this.props.enableRange) {
      this.updateRange({...this.state, ...partialState})
    } else {
      this.props.onChange(value)
    }
  }

  getOnRangeChange (key) {
    let updateFn = (ev) => {
      let partialState = {}
      partialState[key] = ev.target.value
      this.setState(partialState)
      this.updateRange({...this.state, ...partialState})
    }
    if (key !== 'step') {
      return updateFn
    } else {
      return (ev) => {
        let val = ev.target.value
        if ((+val) !== (val|0)) return
        return updateFn(ev)
      }
    }
  }

  toggleRange () {
    this.setState({isRange: !this.state.isRange})
  }

  getInputRangePartial (key) {
    return [
      <span className='input-group-addon'>{key}</span>,
      <input type='number' value={this.state[key]} onChange={this.getOnRangeChange(key)} className='form-control' placeholder='Moles' />
    ]
  }

  render () {
    let rangeBtn = null
    if (this.props.enableRange) {
      rangeBtn =
        <span className='input-group-addon range-btn' onClick={this.toggleRange}>
          <span className={className('fa-fw', this.state.isRange ? 'fa-hashtag' : 'fa-arrows-h')} />
        </span>
    }
    let inputs = []
    if (this.state.isRange) {
      inputs = []
        .concat(this.getInputRangePartial('min'))
        .concat(this.getInputRangePartial('max'))
        .concat(this.getInputRangePartial('step'))
    } else {
      inputs = [
        <input type='number' value={this.state.value} onChange={this.onValueChange} className='form-control' placeholder='Moles' />
      ]
    }
    return <div className='input-group'>
      <span className='input-group-addon bg-primary text-light'>{this.props.name}</span>
      {inputs}
      {rangeBtn}
      <span className='input-group-addon close-btn'>
        <span className='fa-fw fa-close' />
      </span>
    </div>
  }
}

export default InputSpecies
