import React from 'react'
import PropTypes from 'prop-types'
import classname from 'classnames'

class InputWrapper extends React.Component {
  constructor (props) {
    super(props)
    this.props = this.props || {}
    this.state = {
      value: this.props.value || 'Empty',
      focused: false
    }
    this.onChange = this.onChange.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.onBlur = this.onBlur.bind(this)
  }

  onChange (ev) {
    this.setState({value: ev.target.value})
  }

  onFocus (ev) {
    this.setState({focused: true})
  }
  onBlur (ev) {
    this.setState({focused: false})
  }

  render () {
    return <span className={classname([
      'input-wrapper',
      {focus: this.state.focused},
      this.props.className])}>
      <input value={this.state.value} onChange={this.onChange} onFocus={this.onFocus} onBlur={this.onBlur} />
      <span className='input-wrapper-children'>
        {this.props.children}
      </span>
    </span>
  }
}

InputWrapper.propTypes = {
  onUpdate: PropTypes.func,
  className: PropTypes.string
}

export default InputWrapper
