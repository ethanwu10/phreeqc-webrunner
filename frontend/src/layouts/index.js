import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Spacer from '../components/Spacer'

// import './index.css'
import '../styles/index.sass'

const Header = () =>
  <nav
    className='navbar navbar-dark bg-dark'
  >
    <Link
      to='/'
      className='navbar-brand'
    >
      PHREEQC Client
    </Link>
  </nav>

const TemplateWrapper = ({ children }) =>
  <div>
    <Helmet
      title='PHREEQC Web Client'
      meta={[
      ]}
    />
    <Header />
    <Spacer />
    <div
      className='container'
    >
      {children()}
    </div>
  </div>

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
