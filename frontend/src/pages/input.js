import React from 'react'
import InputSpecies from '../components/InputSpecies'
import _omit from 'lodash.omit'
import request from 'superagent'
import path from 'path'

function btnWrap (f) {
  return (ev) => {
    ev.preventDefault()
    return f()
  }
}

class InputPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      qtys: {
        ph: null,
        Zn: null,
        Citrate: null,
        Edta: null
      }
    }

    this.submit = this.submit.bind(this)
  }

  getOnChangeFn (tag) {
    return function (newVal) {
      let newStatePartial = {qtys: this.state.qtys}
      newStatePartial.qtys[tag] = newVal
      this.setState(newStatePartial)
    }.bind(this)
  }

  submit () {
    let qtys = {
      ph: 0,
      Zn: [0],
      Citrate: [0],
      Edta: [0],
      ...this.state.qtys
    }
    // _defaults(qtys, {
    //   ph: 0,
    //   zn: [0],
    //   cit: [0],
    //   edta: [0]
    // })
    this.setState(qtys)

    request
      .post('/api/template/basic.pqi')
      .send({
        ph: qtys.ph,
        qtys: _omit(qtys, ['ph'])
      })
      .end((err, res) => {
        // TODO: err handling
        if (err) return
        let id = path.basename(res.headers['location'])
        this.props.history.push({
          pathname: '/output#' + id
        })
      })
  }

  render () {
    return <form>
      <InputSpecies name='pH' onChange={this.getOnChangeFn('ph')} />
      <br />
      <InputSpecies name='Zn' onChange={this.getOnChangeFn('Zn')} enableRange />
      <br />
      <InputSpecies name='Citrate' onChange={this.getOnChangeFn('Citrate')} enableRange />
      <br />
      <InputSpecies name='Edta' onChange={this.getOnChangeFn('Edta')} enableRange />
      <br />
      <a href='#' className='btn btn-primary' onClick={btnWrap(this.submit)}>
        Submit
      </a>
    </form>
  }
}

export default InputPage
