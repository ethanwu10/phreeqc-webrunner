import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () =>
  <div>
    <Link
      className='btn btn-light'
      to='/input'
    >
      Input
    </Link>
  </div>

export default IndexPage
