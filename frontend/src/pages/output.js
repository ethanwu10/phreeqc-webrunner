import React from 'react'
import {Line} from 'react-chartjs-2'
import request from 'superagent'

const ColorPalette = [
  '#ff5260',
  '#90ff6b',
  '#4879ff',
  '#ffac6f'
]

class OutputPage extends React.Component {
  constructor (props) {
    super(props)

    this.getData = this.getData.bind(this)

    this.state = {
      id: null,
      loading: false
    }

  }

  componentDidMount () {
    let id = window.location.hash.substr(1)
    if (id === '') id = null
    this.setState({
      id: id,
      loading: true
    })

    let url = '/api/template/' + id + '/out.sel'
    request
      .get(url)
      .end((err, res) => {
        if (err) {
          // FIXME
          this.setState({
            loading: false,
            id: null
          })
        } else {
          this.setState({
            loading: false,
            data: res.body
          })
        }
      })
  }

  getData () {
    let getDataset = (labels) => {
      return labels.map((label, index) => {
        return {
          label: label,
          data: this.state.data[label],
          borderColor: ColorPalette[index]
        }
      })
    }

    return {
      labels: this.state.data['Edta'],
      datasets: getDataset([
        'm_Zn+2',
        'm_Zn(Edta)-2',
        'm_Zn(Citrate)-'
      ])
    }
  }

  render () {
    if (!this.state.id) {
      return <span className='fa-4x fa-fw fa-close' style={{'color': 'red'}} />
    }
    if (this.state.loading) {
      return <span className='fa-4x fa-fw fa-spinner fa-spin' />
    } else {
      return <Line data={this.getData} options={{
        multiTooltipTemplate: '<%=datasetLabel%> : <%= value %>',
        tooltips: {
          intersect: false,
          mode: 'x'
        }
      }} />
    }
  }
}

export default OutputPage
