let mongoose = require('mongoose')
const Schema = mongoose.Schema

let runSchema = new Schema({
  template: String,
  input: Object,
  output: Object
})

module.exports = runSchema
