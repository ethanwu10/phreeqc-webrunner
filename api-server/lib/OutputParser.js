const P = require('bluebird')
const csv = require('csv')
const urlJoin = require('url-join')
const config = require('config')
const request = require('request-promise')
const _ = require('lodash')

let OutputParser = {}

OutputParser.parse = (outputs, location) => {
  let promise = P.all(_.map(outputs, (output) => {
    let options = {
      method: 'GET',
      uri: urlJoin(config.get('runner.server'), location, output),
      json: false
    }
    return P.all([request(options), P.resolve(output)])
  }))
    .then((files) =>
      P.all(_.map(files, (file) =>
        P.all([OutputParser.parseCsv(file[0]), P.resolve(file[1])])
      ))
    )
    // .then((files) => {
    //   return _.mapValues(_.keyBy(files, _.curry(_.nth)(_, 1)), _.curry(_.nth)(_, 0))
    // })
    .then((files) =>
      _.flow(_.curry(_.keyBy)(_, _.curry(_.nth)(_, 1)), _.curry(_.mapValues)(_, _.first))(files)
    )
  if (config.get('app.deletePhreeqcRecord')) {
    promise = promise.then((files) => {
      let options = {
        method: 'DELETE',
        uri: urlJoin(config.get('runner.server'), location)
      }
      return P.all([P.resolve(files), request(options)])
    })
      .then(_.spread((files) => files))
  }
  return promise
}

OutputParser.parseCsv = (contents) => {
  const options = {
    auto_parse: true,
    delimiter: ',',
    columns: null
    // trim: true
  }
  contents = contents.replace(/^\s*/, '')
  contents = contents.replace(/\s*$/, '')
  contents = contents.replace(/[ \t]+/g, ',')
  contents = contents.replace(/,\n,/g, '\n')
  return P.promisify(csv.parse)(contents, options)
    .then((data) => _.flow(_.zip, _.curry(_.keyBy)(_, _.head), _.curry(_.mapValues)(_, _.drop))(...data))
    .then((data) => {
      // console.log(data)
      return data
    })
}

module.exports = OutputParser
