const P = require('bluebird')
const express = require('express')
const request = require('request-promise')
const _ = require('lodash')
const memoize = _.memoize
const hbs = require('handlebars')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const path = require('path')
const urlJoin = require('url-join')
const matter = require('gray-matter')
const fs = P.promisifyAll(require('fs'))
const morgan = require('morgan')
const config = require('config')
const OutputParser = require('./lib/OutputParser')
const runSchema = require('./lib/runSchema')

mongoose.Promise = P

let app = express()

const templateDir = path.join(__dirname, 'template')
let RunModel

mongoose.connect(config.get('database.server'), {useMongoClient: true}) // FIXME: obvs
  .then(function () {
    RunModel = mongoose.model('template', runSchema)
  })

function methodNotAllowed (req, res, next) {
  let err = new Error('Method Not Allowed')
  err.status = 405
  next(err)
}

const loadAndCompileTemplate = memoize((path) => {
  return fs.readFileAsync(path)
    .then((data) => {
      let parsedData = matter(data.toString())
      return {
        apply: hbs.compile(parsedData.content),
        data: parsedData.data
      }
    })
})

app.use(morgan('dev'))
app.route('/api/template')
  .get((req, res) => {
    // TODO: template listing
  })
  .all(methodNotAllowed)

function cartesianMix (arg) { // https://stackoverflow.com/a/15310051
  let r = []
  // let arg = arguments
  let max = arg.length - 1
  function helper (arr, i) {
    for (let j = 0, l = arg[i].length; j < l; ++j) {
      let a = arr.slice(0) // clone arr
      a.push(arg[i][j])
      if (i === max) {
        r.push(a)
      } else {
        helper(a, i + 1)
      }
    }
  }
  helper([], 0)
  return r
}

function precompile (data) {
  let output = data
  output.speciesListHeaderList = []
  output.speciesListContentList = []
  for (var species in data.qtys) {
    if (data.qtys.hasOwnProperty(species)) {
      output.speciesListHeaderList.push(species)
      output.speciesListContentList.push(data.qtys[species])
    }
  }
  output.speciesListHeader = output.speciesListHeaderList.join('\t')
  output.speciesListContent = _.map(cartesianMix(output.speciesListContentList), function (l) { return l.join('\t') }).join('\n')
  return output
}

function escapeFile(name) {
  return name.replace(/\./g, '_')
}

function getDocument (id) {
  return function (req, res, next) {
    try {
      req.document = RunModel.findOne({_id: _.get(req, id || 'params.id')})
      next()
    } catch (e) {
      next(e)
    }
  }
}

app.route('/api/template/:name')
  .post(bodyParser.json(), (req, res, next) => {
    loadAndCompileTemplate(path.join(templateDir, req.params.name + '.hbs'))
      .then((template) => {
        let input = template.apply(precompile(req.body))
        let options = {
          method: 'POST',
          uri: urlJoin(config.get('runner.server'), 'run'),
          body: input,
          json: false,
          transform: (body, response, resolveWithFullResponse) => response.headers['location'],
          headers: {
            'content-type': 'application/x-phreeqc-input'
          }
        }
        return P.all([template.data, request(options)])
      })
      .then(_.spread((data, location) => {
        return OutputParser.parse([data.selected_outputs], location)
      }))
      .then((parsedOutput) => new RunModel({
        template: req.params.name,
        input: req.body,
        output: _.mapKeys(parsedOutput, (value, key) => escapeFile(key))
      }).save())
      .then((runDoc) => {
        // FIXME
        // console.log(runDoc)
        res.statusCode = 204
        res.header('location', urlJoin('/api/template/', req.params.name, '/', runDoc._id))
        res.end()
      })
      .catch((err) => {
        next(err)
      })
  })

app.route(/\/api\/template\/([^/]*)\/(.*)/) // FIXME: not at all an atrocious mess
  .get(getDocument('params.[0]'), function (req, res, next) {
    req.document
    // .where(function (run) {
    //   return !!(_.find(run.files, function (file) {
    //     return file.name === req.params[1]
    //   }))
    // })
      .select('output')
      .then(function (run) {
        let fileName = escapeFile(req.params[1])
        let file = run.output[fileName]
        if (file) {
          // console.log(file)
          res.statusCode = 200
          res.json(file)
        } else {
          res.statusCode = 404
          res.end()
        }
      })
      .catch((e) => next(e))
  })
  .all(methodNotAllowed)

app.use(function (req, res, next) { // Catch 404
  let err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use(function (err, req, res, next) { // Error handler
  console.log(err)
  res.status(err.status || 500)
  res.end(err.message) // TODO: keep?
})

module.exports = app
